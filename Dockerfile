# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster

WORKDIR /python-docker

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY . .

ARG var_name=def_val

ENV env_var_name=$var_name

RUN echo "oh dang look at that $env_var_name"

RUN chmod +x /python-docker/start.sh

CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]
